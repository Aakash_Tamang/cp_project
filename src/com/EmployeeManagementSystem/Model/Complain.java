/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Model;

import com.EmployeeManagementSystem.Controller.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Liverpool
 */
public class Complain {
    private int complainid;
    private String complain_subject;
    private String complain;
    private int empid;
    private String date;

    public int getComplainid() {
        return complainid;
    }

    public String getComplain_subject() {
        return complain_subject;
    }

    public String getComplain() {
        return complain;
    }

    public int getEmpid() {
        return empid;
    }
    
    
    public String getDate(){
        return date;
    }

    public void setComplainid(int complainid) {
        this.complainid = complainid;
    }

    public void setComplain_subject(String complain_subject) throws Exception{
        if (complain_subject==null || complain_subject.equals("")){
            throw new Exception ("Plase Enter Complain Subject");
        }
        this.complain_subject = complain_subject;
    }
    

    public void setComplain(String complain) throws Exception{
        if (complain==null || complain.equals("")){
            throw new Exception ("Plase Enter Complain");
        }
        this.complain = complain;
    }  

    public void setEmpid(int empid) {
        this.empid = empid;
    }
    
        
    public void setDate(String date){
        this.date = date;
    }
    
    public boolean addNewComplain(){
        Connection conn;
        Statement st;
        String query = "INSERT INTO complain (Complain_Subject, Complain, Emp_ID, Date) VALUES ('"+complain_subject+"','"+complain+"','"+empid+"','"+date+"')";
        conn = DbConnection.createConnection();
        System.out.println("Connection with Database Done");
        try{
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            if(res>0){
                System.out.println("Created");
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        System.out.println("Not Created");
        return false;
    }    
    
    public ResultSet viewAllComplain(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT complain.Complain_ID, employee.FirstName, employee.LastName, complain.Emp_ID, employee.Department, employee.Role, complain.Complain_Subject, complain.Complain, complain.Date FROM complain INNER JOIN employee ON complain.Emp_ID = employee.Emp_ID";
            //String query = "SELECT complain.Complain_ID, employee.FirstName, employee.LastName, employee.Emp_ID, employee.Department, employee.Role, complain.Complain_Subject, complain.Complain, complain.Date FROM complain , employee";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        System.out.println("HERE");
        return null;
    }
    
    public ResultSet viewSentComplain(int id){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            System.out.println(id);
            String query = "SELECT complain.Complain_ID, employee.FirstName, employee.LastName, complain.Emp_ID, employee.Department, employee.Role, complain.Complain_Subject, complain.Complain, complain.Date FROM complain INNER JOIN employee ON complain.Emp_ID = employee.Emp_ID WHERE complain.Emp_ID = '"+id+"' ";  
            //String query = "SELECT complain.Complain_ID, employee.FirstName, employee.LastName, complain.Emp_ID, employee.Department, employee.Role, complain.Complain_Subject, complain.Complain, complain.Date FROM complain, employee WHERE complain.Emp_ID = employee.Emp_ID"; 
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public int updateComplain (){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            String query = "UPDATE complain SET Complain_Subject = '"+complain_subject+"', Complain = '"+complain+"', Date = '"+date+"' WHERE Complain_ID = '"+complainid+"'";
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public int removeComplain(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            String query = "DELETE FROM complain WHERE Complain_ID = '"+id+"'";
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Delete");
        return 0;
    }     
    
    
}
