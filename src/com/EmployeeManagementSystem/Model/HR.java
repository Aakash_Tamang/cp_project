/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Model;

import com.EmployeeManagementSystem.Controller.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Liverpool
 */
public class HR {
    private int hrid;
    private String fname;
    private String mname;
    private String lname;
    private String DOB;
    private String Gender;
    private String address;
    private String contact;
    private String email;
    private int deptid;
    private int roleid;
    private String username;
    private String password;
    private String status;

    public int getHrid() {
        return hrid;
    }

    public String getFname() {
        return fname;
    }

    public String getMname() {
        return mname;
    }

    public String getLname() {
        return lname;
    }

    public String getDOB() {
        return DOB;
    }
    
    public String getGender() {
        return Gender;
    }

    public String getAddress() {
        return address;
    }

    public String getContact() {
        return contact;
    }

    public String getEmail() {
        return email;
    }

    public int getDeptid() {
        return deptid;
    }
    
    public int getRoleid() {
        return roleid;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getStatus() {
        return status;
    }

    public void setHrid(int hrid) {
        this.hrid = hrid;
    }

    public void setFname(String fname) throws Exception{
        if (fname==null || fname.equals("")){
            throw new Exception ("FirstName cannot be blank");
        }
        this.fname = fname;
    }

    public void setLname(String lname) throws Exception {
        if (lname==null || lname.equals("")){
            throw new Exception ("LastName cannot be blank");
        }
        this.lname = lname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public void setDOB(String DOB) throws Exception{
        if (DOB==null || DOB.equals("")){
            throw new Exception ("Date cannot be blank");
        }
        this.DOB = DOB;
    }
    
    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public void setAddress(String address) throws Exception{
        if (address==null || address.equals("")){
            throw new Exception ("Address cannot be blank");
        }
        this.address = address;
    }

    public void setContact(String contact) throws Exception{
        if (contact==null || contact.equals("")){
            throw new Exception ("Contact cannot be blank");
        }
        this.contact = contact;
    }

    public void setEmail(String email) throws Exception{
        if (email==null || email.equals("")){
            throw new Exception ("Email cannot be blank");
        }
        this.email = email;
    }

    public void setDeptid(int deptid) {
        this.deptid = deptid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public void setUsername(String username) throws Exception{
        if (username==null || username.equals("")){
            throw new Exception ("Username cannot be blank");
        }
        this.username = username;
    }

    public void setPassword(String password) throws Exception{
        if (password==null || password.equals("")){
            throw new Exception ("Password cannot be blank");
        }
        this.password = password;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public ResultSet hrLogin(){
        Connection conn; 
        Statement st;
        ResultSet res;
        String query = "SELECT * FROM hr WHERE UserName = '"+username+"' AND Password = '"+password+"' AND Status = 'Approved' ";
        conn = DbConnection.createConnection();  
        try{
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        }catch (Exception ex){
            System.out.println("Error");
        }
        return null;
    }
    public int updateHR (){
        try{
            Connection conn;
            Statement st;
            //String query = "UPDATE hr SET FirstName = '"+fname+"', MiddleName = '"+mname+"', LastName = '"+lname+"', DOB = '"+DOB+"', Gender = '"+Gender+"', Address = '"+address+"', Contact = '"+contact+"', Email = '"+email+"', UserName = '"+username+"', Password = '"+password+"' WHERE HR_ID = '"+hrid+"'";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            //System.out.println("Going to check query");
            String query = "UPDATE hr SET FirstName = '"+fname+"', MiddleName = '"+mname+"', LastName = '"+lname+"', DOB = '"+DOB+"', Gender = '"+Gender+"', Address = '"+address+"', Contact = '"+contact+"', Email = '"+email+"', UserName = '"+username+"', Password = '"+password+"' WHERE HR_ID = '"+hrid+"'";
            int res = st.executeUpdate(query);
            System.out.println(res);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public ResultSet viewSelf(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT * FROM hr WHERE HR_ID = '"+hrid+"'";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    public boolean addHR(){
        Connection conn;
        Statement st;
        /*System.out.println(fname);
        System.out.println(mname);
        System.out.println(lname);
        System.out.println(DOB);
        System.out.println(Gender);
        System.out.println(address);
        System.out.println(contact);
        System.out.println(email);System.out.println(username);
        System.out.println(password);*/          
        
        String query = "INSERT INTO hr (FirstName, MiddleName, LastName, DOB, Gender, Address, Contact, Email, UserName, Password, Status) VALUES ('"+fname+"','"+mname+"','"+lname+"','"+DOB+"','"+Gender+"','"+address+"','"+contact+"','"+email+"','"+username+"','"+password+"','Pending')";
        //String query = "INSERT INTO `hr`(`FirstName`, `MiddleName`, `LastName`, `DOB`, `Gender`, `Address`, `Contact`, `Email`, `UserName`, `Password`, `Status`) VALUES ('"+fname+"','"+mname+"','"+lname+"','"+DOB+"','"+Gender+"','"+address+"','"+contact+"','"+email+"','"+username+"','"+password+"','P')";
        conn = DbConnection.createConnection();
        //System.out.println("Connection with Database Done");
        try{
            st = conn.createStatement();
            //System.out.println("Going to check query");
            int res = st.executeUpdate(query);
            //System.out.println("Going to check query");
            //System.out.println(res);
            if(res>0){
                System.out.println("Inserted");
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        System.out.println("Not Inserted");
        return false;
    }
    public int acceptHR(String hrid){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();            
            System.out.println("Going to run query");
            String query = "UPDATE hr SET Status = 'Approved' WHERE HR_ID = '"+hrid+"'";
            System.out.println("Running query");
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public ResultSet viewHR(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Status FROM hr";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    /*public ResultSet viewSelfHR(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT * FROM hr WHERE HR_ID = '"+hrid+"'";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }*/
    public ResultSet viewSelectedHR(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Status FROM hr WHERE Status = 'Approved'";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    public ResultSet viewNotSelectedHR(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Status FROM hr WHERE Status = 'Pending'";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    public ResultSet searchHR(String name){
        try{
            Connection conn;
            Statement st;
            ResultSet res;
            conn = DbConnection.createConnection();
            st = conn.createStatement();
            String query = "SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Status FROM hr WHERE FirstName LIKE '%"+name+"%'";
            res = st.executeQuery(query);
            return res;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public int revokeHR(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
                        System.out.println("Going to run query");
            String query = "UPDATE hr SET Status = 'Pending' WHERE HR_ID = '"+id+"'";
                        System.out.println("Running query");
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Revoke");
        return 0;
    }
    /*public int removeHR(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            String query = "DELETE FROM hr WHERE HR_ID = '"+id+"'";
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Remove");
        return 0;
    }*/ 

    
    
}
