/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Model;

import com.EmployeeManagementSystem.Controller.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Liverpool
 */
public class Admin_Order {
    private int taskid;
    private String task_subject;
    private String task;
    private String date;
    private String help;

    public int getTaskid() {
        return taskid;
    }
    
    public String getTask_subject() {
        return task_subject;
    }

    public String getTask() {
        return task;
    }

    public String getDate() {
        return date;
    }


    public void setTaskid(int taskid) {
        this.taskid = taskid;
    }

    public void setTask_subject(String task_subject) throws Exception{
        if (task_subject==null || task_subject.equals("")){
            throw new Exception ("Please Enter Task Subject");
        }
        this.task_subject = task_subject;
    }

    public void setTask(String task) throws Exception{
        if (task==null || task.equals("")){
            throw new Exception ("Please Enter Task Details");
        }
        this.task = task;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    public boolean addNewTask(){
        Connection conn;
        Statement st;
        String query = "INSERT INTO admin_order (Task_Subject, Task, Date) VALUES ('"+task_subject+"','"+task+"','"+date+"')";
        conn = DbConnection.createConnection();
        System.out.println("Connection with Database Done");
        try{
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            if(res>0){
                System.out.println("Created");
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        System.out.println("Not Created");
        return false;
    }   
    
    
    public ResultSet viewTask(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT * from admin_order";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public int updateTask(){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            String query = "UPDATE admin_order SET Task = '"+task+"' WHERE Task_ID = '"+taskid+"' ";
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public int removeTask(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            String query = "DELETE FROM admin_order WHERE Task_ID = '"+id+"'";
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Delete");
        return 0;
    }
    
    
    
}
