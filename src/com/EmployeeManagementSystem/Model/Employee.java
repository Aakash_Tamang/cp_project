/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Model;

import com.EmployeeManagementSystem.Controller.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Liverpool
 */
public class Employee {
    private int empid;
    private String fname;
    private String mname;
    private String lname;
    private String DOB;
    private String Gender;
    private String address;
    private String contact;
    private String email;
    private String dept;
    private String role;
    private int deptid;
    private int roleid;
    private String username;
    private String password;
    private String status;
    
    public int getEmpid() {
        return empid;
    }

    public String getFname() {
        return fname;
    }

    public String getMname() {
        return mname;
    }

    public String getLname() {
        return lname;
    }

    public String getDOB() {
        return DOB;
    }

    public String getGender() {
        return Gender;
    }

    public String getAddress() {
        return address;
    }

    public String getContact() {
        return contact;
    }

    public String getEmail() {
        return email;
    }

    public String getDept() {
        return dept;
    }

    public String getRole() {
        return role;
    }

    public int getDeptid() {
        return deptid;
    }
    
    public int getRoleid() {
        return roleid;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getStatus() {
        return status;
    }

    public void setEmpid(int empid) {
        this.empid = empid;
    }

    public void setFname(String fname) throws Exception{
        if (fname==null || fname.equals("")){
            throw new Exception ("FirstName cannot be blank");
        }
        this.fname = fname;
    }

    public void setLname(String lname) throws Exception {
        if (lname==null || lname.equals("")){
            throw new Exception ("LastName cannot be blank");
        }
        this.lname = lname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public void setDOB(String DOB) throws Exception{
        if (DOB==null || DOB.equals("")){
            throw new Exception ("Date cannot be blank");
        }
        this.DOB = DOB;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public void setAddress(String address) throws Exception{
        if (address==null || address.equals("")){
            throw new Exception ("Address cannot be blank");
        }
        this.address = address;
    }

    public void setContact(String contact) throws Exception{
        if (contact==null || contact.equals("")){
            throw new Exception ("Contact cannot be blank");
        }
        this.contact = contact;
    }

    public void setEmail(String email) throws Exception{
        if (email==null || email.equals("")){
            throw new Exception ("Email cannot be blank");
        }
        this.email = email;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setDeptid(int deptid) {
        this.deptid = deptid;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public void setUsername(String username) throws Exception{
        if (username==null || username.equals("")){
            throw new Exception ("Username cannot be blank");
        }
        this.username = username;
    }

    public void setPassword(String password) throws Exception{
        if (password==null || password.equals("")){
            throw new Exception ("Password cannot be blank");
        }
        this.password = password;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public ResultSet eLogin(){
        Connection conn; 
        Statement st;
        ResultSet res;
        String query = "SELECT * FROM employee WHERE UserName = '"+username+"' AND Password = '"+password+"' AND Status = 'Approved' ";
        conn = DbConnection.createConnection();  
        try{
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        }catch (Exception ex){
            System.out.println("Error");
        }
        return null;
    }
    public int updateEmployee (){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            String query = "UPDATE employee SET FirstName = '"+fname+"', MiddleName = '"+mname+"', LastName = '"+lname+"', DOB = '"+DOB+"', Gender = '"+Gender+"', Address = '"+address+"', Contact = '"+contact+"', Email = '"+email+"', UserName = '"+username+"', Password = '"+password+"' WHERE Emp_ID = '"+empid+"'";
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int updateEmployeeStatus (){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            String query = "UPDATE employee SET Department = '"+dept+"', Role = '"+role+"', Status = '"+status+"' WHERE Emp_ID = '"+empid+"'";
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public ResultSet viewSelf(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT * FROM employee WHERE EMp_ID = '"+empid+"'";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    public boolean addEmployee(){
        Connection conn;
        Statement st;
        String query = "INSERT INTO employee (FirstName, MiddleName, LastName, DOB, Gender, Address, Contact, Email, UserName, Password,Department,Role, Status) VALUES ('"+fname+"','"+mname+"','"+lname+"','"+DOB+"','"+Gender+"','"+address+"','"+contact+"','"+email+"','"+username+"','"+password+"','"+dept+"','"+role+"','Pending')";
        conn = DbConnection.createConnection();
        System.out.println("Connection with Database Done");
        try{
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            if(res>0){
                System.out.println("Inserted");
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        System.out.println("Not Inserted");
        return false;
    }
    public int acceptEmployee(String empid){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st = conn.createStatement();
            String query = "UPDATE employee SET Status = 'Approved' WHERE Emp_ID = '"+empid+"'";
            int res = st.executeUpdate(query);
            return res;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public ResultSet viewEmployee(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Department,Role,Status FROM employee e";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    /*public ResultSet viewSelfHR(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT * FROM hr WHERE HR_ID = '"+hrid+"'";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }*/
    public ResultSet viewSelectedEmployee(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Department,Role,Status FROM employee WHERE Status = 'Approved'";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    public ResultSet viewNotSelectedEmployee(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Department,Role,Status FROM employee WHERE Status = 'Pending'";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    public ResultSet searchEmployee(String name){
        try{
            Connection conn;
            Statement st;
            ResultSet res;
            conn = DbConnection.createConnection();
            st = conn.createStatement();
            String query = "SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Department,Role,Status FROM employee WHERE FirstName LIKE '%"+name+"%'";
            res = st.executeQuery(query);
            return res;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public ResultSet searchSelectedEmployee(String name){
        try{
            Connection conn;
            Statement st;
            ResultSet res;
            conn = DbConnection.createConnection();
            st = conn.createStatement();
            String query = "SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Department,Role,Status FROM employee WHERE FirstName LIKE '%"+name+"%' AND Status = 'Approved'";
            res = st.executeQuery(query);
            return res;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public ResultSet searchNotSelectedEmployee(String name){
        try{
            Connection conn;
            Statement st;
            ResultSet res;
            conn = DbConnection.createConnection();
            st = conn.createStatement();
            String query = "SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Department,Role,Status FROM employee WHERE FirstName LIKE '%"+name+"%' AND Status = 'Pending'";
            res = st.executeQuery(query);
            return res;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public int revokeEmployee(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            String query = "UPDATE employee SET Status = 'Pending' WHERE Emp_ID = '"+id+"'";
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Revoke");
        return 0;
    }
    public int removeEmployee(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            String query = "DELETE FROM employee WHERE Emp_ID = '"+id+"'";
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Remove");
        return 0;
    }
    

}
