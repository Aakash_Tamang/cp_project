/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Model;

import com.EmployeeManagementSystem.Controller.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Liverpool
 */
public class Notice {
    private int noticeid;
    private String notice_subject;
    private String notice;
    private int hrid;
    private String date;

    public int getNoticeid() {
        return noticeid;
    }

    public String getNotice_subject() {
        return notice_subject;
    }

    public String getNotice() {
        return notice;
    }

    public int getHrid() {
        return hrid;
    }
    
    
    public String getDate(){
        return date;
    }

    public void setNoticeid(int noticeid) {
        this.noticeid = noticeid;
    }

    public void setNotice_subject(String notice_subject) throws Exception{
        if (notice_subject==null || notice_subject.equals("")){
            throw new Exception ("Plase Enter Notice Subject");
        }
        this.notice_subject = notice_subject;
    }

    public void setNotice(String notice) throws Exception{
        if (notice==null || notice.equals("")){
            throw new Exception ("Plase Enter Notice Details");
        }
        this.notice = notice;
    }

    public void setHrid(int hrid) {
        this.hrid = hrid;
    }
    
    
    public void setDate(String date){
        this.date = date;
    }
    
    public boolean addNewNotice(){
        Connection conn;
        Statement st;
        String query = "INSERT INTO notice (Notice_Subject, Notice, HR_ID, Date) VALUES ('"+notice_subject+"','"+notice+"','"+hrid+"','"+date+"')";
        conn = DbConnection.createConnection();
        System.out.println("Connection with Database Done");
        try{
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            if(res>0){
                System.out.println("Created");
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        System.out.println("Not Created");
        return false;
    }    
    
    public ResultSet viewAllNotice(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT notice.Notice_ID, hr.FirstName, hr.LastName, notice.HR_ID, notice.Notice_Subject, notice.Notice, notice.Date FROM notice INNER JOIN hr ON notice.HR_ID = hr.HR_ID";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public ResultSet viewNotice(int id){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT notice.Notice_ID, hr.FirstName, hr.LastName, notice.HR_ID, notice.Notice_Subject, notice.Notice, notice.Date FROM notice INNER JOIN hr on notice.HR_ID = hr.HR_ID WHERE notice.HR_ID = '"+id+"' ";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public int updateNotice (){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            String query = "UPDATE notice SET Notice_Subject = '"+notice_subject+"', Notice = '"+notice+"', Date = '"+date+"' WHERE Notice_ID = '"+noticeid+"'";
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    public int removeNotice(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            String query = "DELETE FROM notice WHERE Notice_ID = '"+noticeid+"'";
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Delete");
        return 0;
    }  
    
}
