/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Model;

import com.EmployeeManagementSystem.Controller.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Liverpool
 */
public class Department {
    private int deptid;
    private String department;
    private String status;

    public int getDeptid() {
        return deptid;
    }

    public String getDepartment() {
        return department;
    }

    public String getStatus() {
        return status;
    }
    
    

    public void setDeptid(int deptid) {
        this.deptid = deptid;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    

    public void setDepartment(String department) throws Exception{
        if (department==null || department.equals("")){
            throw new Exception ("Department Name Cannot be Empty");
        }
        this.department = department;
    }
    
    public boolean addNewDepartment(){
        Connection conn;
        Statement st;
        String query = "INSERT INTO department (Department) VALUES ('"+department+"')";
        conn = DbConnection.createConnection();
        System.out.println("Connection with Database Done");
        try{
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            if(res>0){
                System.out.println("Created");
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        System.out.println("Not Created");
        return false;
    }    
    
    public ResultSet viewDepartment(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT * from department";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public int updateDepartment (){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            String query = "UPDATE department SET Department = '"+department+"' WHERE Dept_ID = '"+deptid+"'";
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    /*public int revokeDepartment(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            String query = "UPDATE department SET Status = 'P' WHERE Dept_ID = '"+id+"'";
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Delete");
        return 0;
    }*/
        
}
