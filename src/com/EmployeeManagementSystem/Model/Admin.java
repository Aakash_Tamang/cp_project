/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Model;

import com.EmployeeManagementSystem.Controller.AdminLogic;
import com.EmployeeManagementSystem.Controller.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Liverpool
 */
public class Admin {
    private int aid;
    private String username;
    private String password;

    public int getAid() {
        return aid;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }
    

    public void setUsername(String username) throws Exception {
        if (username==null || username.equals("")){
            throw new Exception ("Username cannot be blank");
        }
        this.username = username;
    }

    public void setPassword(String password) throws Exception {
        if (password==null || password.equals("")){
            throw new Exception ("Password cannot be blank");
        }
        this.password = password;
    }
    
    public ResultSet adLogin(){
        Connection conn; 
        Statement st;
        ResultSet res;
        String query = "SELECT * FROM admin WHERE UserName = '"+username+"' AND Password = '"+password+"'";
        conn = DbConnection.createConnection();  
        try{
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        }catch (Exception ex){
            System.out.println("Error");
        }
        return null;
    }
    
    public int updateAdmin (){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            String query = "UPDATE admin SET UserName = '"+username+"', Password = '"+password+"' WHERE Admin_ID = '"+aid+"' ";
            int res = st.executeUpdate(query);
            System.out.println(res);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public ResultSet viewSelf(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT * FROM admin WHERE Admin_ID = '"+aid+"'";
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    
}
