/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Model;

import com.EmployeeManagementSystem.Controller.DbConnection;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Liverpool
 */
public class Role {
    private int roleid;
    private String role;

    public int getRoleid() {
        return roleid;
    }

    public String getRole() {
        return role;
    }

    public void setRoleid(int roleid) {
        this.roleid = roleid;
    }

    public void setRole(String role) throws Exception{
        if (role==null || role.equals("")){
            throw new Exception ("Role Name cannot be Empty");
        }
        this.role = role;
    }
 
    
    public boolean addNewRole(){
        Connection conn;
        Statement st;
        String query = "INSERT INTO role (Role) VALUES ('"+role+"')";
        conn = DbConnection.createConnection();
        System.out.println("Connection with Database Done");
        try{
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            if(res>0){
                System.out.println("Created");
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        System.out.println("Not Created");
        return false;
    }    
    
    public ResultSet viewRole(){
        try {
            Connection conn;
            Statement st;
            ResultSet res;
            String query = "SELECT * from role";  
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            res = st.executeQuery(query);
            return res;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public int updateRole (){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            st=conn.createStatement();
            String query = "UPDATE role SET Role = '"+role+"' WHERE Role_ID = '"+roleid+"'";
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        return 0;
    }
    
    /*public int revokeRole(String id){
        try{
            Connection conn;
            Statement st;
            conn = DbConnection.createConnection();
            String query = "UPDATE role SET Status = 'P' WHERE Role_ID = '"+id+"'";
            st = conn.createStatement();
            int res = st.executeUpdate(query);
            return res;
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
        }
        System.out.println("Cannot Delete");
        return 0;
    }*/
    
}
