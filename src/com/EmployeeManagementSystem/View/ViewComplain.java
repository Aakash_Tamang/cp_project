/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.View;

import com.EmployeeManagementSystem.Controller.ComplainLogic;
import com.EmployeeManagementSystem.Model.Complain;
import com.EmployeeManagementSystem.Model.Employee;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Liverpool
 */
public class ViewComplain extends javax.swing.JInternalFrame {

    /**
     * Creates new form ViewComplain
     */
    DefaultTableModel table;
    int sn;
    Complain c;
    Employee e;
    ComplainLogic clc;
    public ViewComplain() {
        initComponents();
        c = new Complain();
        e = new Employee();
        clc = new ComplainLogic(c);
        btnView.setEnabled(false);
        table = (DefaultTableModel)tblComplain.getModel();
        ArrayList<String[]>data = clc.viewAllComplain();
        for(String[] dt:data){
            table.addRow(dt);
        }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblComplain = new javax.swing.JTable();
        btnView = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);

        jLabel1.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel1.setText("Complaints");

        tblComplain.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Complain_ID", "First_Name", "LastName", "Emp_ID", "Department", "Role", "Compplain_Subject", "Complain", "Date"
            }
        ));
        tblComplain.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblComplainMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblComplain);

        btnView.setText("View");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 973, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(321, 321, 321)
                .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1)
                        .addGap(34, 34, 34)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        // TODO add your handling code here:
        //c.setComplainid(Integer.parseInt(tblComplain.getValueAt(sn, 0).toString()));
        try{
            c.setComplainid(Integer.parseInt(tblComplain.getValueAt(sn, 0).toString()));
            e.setFname(tblComplain.getValueAt(sn, 1).toString());
            e.setLname(tblComplain.getValueAt(sn, 2).toString());
            e.setEmpid(Integer.parseInt(tblComplain.getValueAt(sn, 3).toString()));
            e.setDept(tblComplain.getValueAt(sn, 4).toString());
            e.setRole(tblComplain.getValueAt(sn, 5).toString());
            c.setComplain_subject(tblComplain.getValueAt(sn, 6).toString());
            c.setComplain(tblComplain.getValueAt(sn, 7).toString());
            c.setDate(tblComplain.getValueAt(sn, 8).toString());
        }catch(Exception ex){
            System.out.println("Cannot View");
            JOptionPane.showMessageDialog(null, "Choose a row");
            //JOptionPane.showMessageDialog(null, ex.getMessage());
        }
        ComplainView cv = new ComplainView(c,e);
        getParent().add(cv);
        cv.setVisible(true);
        
    }//GEN-LAST:event_btnViewActionPerformed

    private void tblComplainMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblComplainMouseClicked
        // TODO add your handling code here:
        btnView.setEnabled(true);
        sn = tblComplain.getSelectedRow();
    }//GEN-LAST:event_tblComplainMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnView;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblComplain;
    // End of variables declaration//GEN-END:variables
}
