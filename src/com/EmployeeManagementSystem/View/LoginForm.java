/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.View;

import com.EmployeeManagementSystem.Controller.AdminLogic;
import com.EmployeeManagementSystem.Controller.DbConnection;
import com.EmployeeManagementSystem.Controller.DepartmentLogic;
import com.EmployeeManagementSystem.Controller.EmployeeLogic;
import com.EmployeeManagementSystem.Controller.HRLogic;
import com.EmployeeManagementSystem.Controller.NoticeLogic;
import com.EmployeeManagementSystem.Controller.RoleLogic;
import com.EmployeeManagementSystem.Controller.Admin_OrderLogic;
import com.EmployeeManagementSystem.Controller.ComplainLogic;
import com.EmployeeManagementSystem.Model.Admin;
import com.EmployeeManagementSystem.Model.Department;
import com.EmployeeManagementSystem.Model.Employee;
import com.EmployeeManagementSystem.Model.HR;
import com.EmployeeManagementSystem.Model.Role;
import com.EmployeeManagementSystem.Model.Admin_Order;
import com.EmployeeManagementSystem.Model.Complain;
import com.EmployeeManagementSystem.Model.Notice;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class LoginForm extends javax.swing.JFrame {

    /**
     * Creates new form LoginForm
     */
    Admin ad;
    HR hr;
    Employee e;
    AdminLogic adlc;
    HRLogic hrlc;
    EmployeeLogic elc;
    String username;
    String password;
    String role;
    //private DbConnection conn;
    public LoginForm() {
        //conn = DbConnection.getInstance();
        initComponents();
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtUserName = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnLogin = new javax.swing.JButton();
        openEmpSignUp = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cmbRole = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        txtPassword = new javax.swing.JPasswordField();
        openHRSignUp = new javax.swing.JButton();

        jLabel6.setText("jLabel6");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login Form");
        setBackground(new java.awt.Color(255, 255, 255));
        setForeground(java.awt.Color.gray);
        setLocation(new java.awt.Point(200, 150));

        jLabel1.setText("Enter User Name");

        jLabel2.setText("Enter Password");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setText("Employee Management System");

        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });

        openEmpSignUp.setText("Employee Account");
        openEmpSignUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openEmpSignUpActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Create A New Account");

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/EmployeeManagementSystem/View/login.png"))); // NOI18N

        cmbRole.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Admin", "Human Resource", "Employee" }));

        jLabel5.setText("Login As");

        openHRSignUp.setText("HR Account");
        openHRSignUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openHRSignUpActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtUserName)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 116, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(openHRSignUp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(openEmpSignUp, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(32, 32, 32))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(openHRSignUp, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(openEmpSignUp, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtUserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbRole, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addComponent(btnLogin, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        // TODO add your handling code here:
        ad = new Admin();
        hr = new HR();
        e = new Employee();
        adlc = new AdminLogic(ad);
        hrlc = new HRLogic(hr);
        elc = new EmployeeLogic(e);        
        username = txtUserName.getText();
        password = txtPassword.getText();
        role = cmbRole.getSelectedItem().toString();
        
        if(role.equalsIgnoreCase("Admin")){
            System.out.println("Admin");
            try {
                ad.setUsername(username);
                ad.setPassword(password);
            
            if(adlc.adLogin()){
                
                JOptionPane.showMessageDialog(null, "You have logged in as Admin");
                HomeAdmin had = new HomeAdmin(ad);
                had.setVisible(true);
                //System.out.println("DONE");
                this.dispose();
            }else{
                System.out.println("Login Error");
                JOptionPane.showMessageDialog(null, "Invalid Username or Password");
                
            }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage());
                System.out.println(ex.getMessage());
            }
        }else if(role.equalsIgnoreCase("Human Resource")){
            System.out.println("Human Resource");
            try {
                hr.setUsername(username);
                hr.setPassword(password);
                if(hrlc.hrLogin()){
                    JOptionPane.showMessageDialog(null, "You have logged in as HR");
                    HomeHR hhr = new HomeHR(hr);
                    hhr.setVisible(true);
                    this.dispose();
                }else{
                    JOptionPane.showMessageDialog(null, "Invalid Username or Password");                
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage());
                System.out.println(ex.getMessage());
            }
        }else{
            System.out.println("Employee");
            try {
                e.setUsername(username);
                e.setPassword(password); 
                if(elc.eLogin()){
                    JOptionPane.showMessageDialog(null, "You have Logged in as Employee");
                    HomeEmployee he = new HomeEmployee(e);
                    he.setVisible(true);
                    this.dispose();
                }else{
                    JOptionPane.showMessageDialog(null, "Invalid Username or Password");
                }
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, ex.getMessage());
                System.out.println(ex.getMessage());
            }
        }
        txtUserName.setText("");
        txtUserName.requestFocus();
        txtPassword.setText("");
    }//GEN-LAST:event_btnLoginActionPerformed

    private void openHRSignUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openHRSignUpActionPerformed
        // TODO add your handling code here:
        //System.out.println("HR");
        SignUpHR sh = new SignUpHR();
        //getParent().add(sh);
        sh.setVisible(true);
    }//GEN-LAST:event_openHRSignUpActionPerformed

    private void openEmpSignUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openEmpSignUpActionPerformed
        // TODO add your handling code here:
        //System.out.println("EMPLOYEE");
        SignUpEmployee se = new SignUpEmployee();
        //getParent().add(se);
        se.setVisible(true);
    }//GEN-LAST:event_openEmpSignUpActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLogin;
    private javax.swing.JComboBox cmbRole;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JButton openEmpSignUp;
    private javax.swing.JButton openHRSignUp;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtUserName;
    // End of variables declaration//GEN-END:variables
}
