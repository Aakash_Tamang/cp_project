/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Controller;

import com.EmployeeManagementSystem.Model.Admin_Order;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class Admin_OrderLogic {
    
    Admin_Order t;
    public Admin_OrderLogic(Admin_Order t){
        this.t = t;
    }
    
    public boolean addNewTask(){
        return t.addNewTask();
    }
       
    public ArrayList<String []> viewTask(){
        ResultSet res;
        res = t.viewTask();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3),res.getString(4)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }  
    
    public boolean updateTask(){
        int res=t.updateTask();
        return res>0;
    }
    
    public boolean removeTask(String id){
        int res = t.removeTask(id);
        return res>0;
    }
    
    
    /*
    private DbConnection conn;
    public Admin_OrderLogic(){
        conn = DbConnection.getInstance();
    }
    
    public boolean addNewTask(Admin_Order t){
        String query = "INSERT INTO admin_order (Task_Subject, Task, Date) Values ('"+t.getTask_subject()+"','"+t.getTask()+"','"+t.getDate()+"')";
        return conn.iud(query)>0;
    }
    
    public ArrayList<String[]> viewTask(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT * from admin_order");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }   
    
    public boolean updateTask (Admin_Order t){
        String query = "UPDATE admin_order SET Task = '"+t.getTask()+"' WHERE Task_ID = '"+t.getTaskid()+"' ";
        JOptionPane.showMessageDialog(null, "The respective Task has been Updated");
        return conn.iud(query)>0;
    }
    
    public boolean removeTask(String taskid, Admin_Order d){
        //String query = "DELETE FROM role WHERE Role_ID = '"+d.getRoletid()+"'";
        String query = "DELETE FROM admin_order WHERE Task_ID = '"+taskid+"'";
        JOptionPane.showMessageDialog(null, "The respective Task has been Removed");
        return conn.iud(query)>0;
    }*/
    
}
