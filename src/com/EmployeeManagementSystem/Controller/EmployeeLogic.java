/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Controller;

import com.EmployeeManagementSystem.Model.Employee;
import com.EmployeeManagementSystem.Model.HR;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class EmployeeLogic {
    
    Employee e;
    public EmployeeLogic(Employee e){
        this.e = e;
    }
    
    public boolean eLogin() throws Exception{
        ResultSet res = e.eLogin();
        try{
            if(res.next()){
                e.setEmpid(Integer.parseInt(res.getString(1)));
                e.setFname(res.getString(2));
                e.setMname(res.getString(3));
                e.setLname(res.getString(4));
                e.setDOB(res.getString(5));
                e.setGender(res.getString(6));
                e.setAddress(res.getString(7));
                e.setContact(res.getString(8));
                e.setEmail(res.getString(9));
                e.setUsername(res.getString(10));
                e.setPassword(res.getString(11));
                e.setDept(res.getString(12));
                e.setRole(res.getString(13));
                e.setStatus(res.getString(14));
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        return false;
    }
    
    public boolean updateEmployee(){
        int res=e.updateEmployee();
        return res>0;
    }
    
    public boolean updateEmployeeStatus(){
        int res=e.updateEmployeeStatus();
        return res>0;
    }
    
    public ArrayList<String []> viewSelf(){
        ResultSet res;
        res = e.viewSelf();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12),res.getString(13),res.getString(14)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public boolean addEmmployee(){
        return e.addEmployee();
    }
    
    public boolean acceptEmployee(String empid){
        int res = e.acceptEmployee(empid);
        return res>0;
    }
    
    public ArrayList<String []> viewEmployee(){
        ResultSet res;
        res = e.viewEmployee();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            //JOptionPane.showMessageDialog(null, "ERROR");
            System.out.println(ex.getMessage());
        }
        return null;
    }
    public ArrayList<String []> viewSelectedEmployee(){
        ResultSet res;
        res = e.viewSelectedEmployee();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public ArrayList<String []> viewNotSelectedEmployee(){
        ResultSet res;
        res = e.viewNotSelectedEmployee();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public ArrayList<String []> searchEmployee(String name){
        ResultSet res;
        res = e.searchEmployee(name);
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public ArrayList<String []> searchSelectedEmployee(String name){
        ResultSet res;
        res = e.searchSelectedEmployee(name);
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    
    public ArrayList<String []> searchNotSelectedEmployee(String name){
        ResultSet res;
        res = e.searchNotSelectedEmployee(name);
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public boolean revokeEmployee(String id){
        int res = e.revokeEmployee(id);
        return res>0;
    }
    
    public boolean removeEmployee(String id){
        int res = e.removeEmployee(id);
        return res>0;
    }    
    
    
    /*private DbConnection conn;
    
    public EmployeeLogic(){
        conn = DbConnection.getInstance();
    }
    
    private int id;
    
    public boolean loginEmployee(Employee e){
        ResultSet res;
        String query = "SELECT * FROM employee WHERE UserName = '"+e.getUsername()+"' AND Password = '"+e.getPassword()+"' AND Action = 'A'";
        res = conn.select(query);
        try{
            if(res.next()){
                e.setEmpid(Integer.parseInt(res.getString(1)));
                e.setFname(res.getString(2));
                e.setMname(res.getString(3));
                e.setLname(res.getString(4));
                e.setDOB(res.getString(5));
                e.setGender(res.getString(6));
                e.setAddress(res.getString(7));
                e.setContact(res.getString(8));
                e.setEmail(res.getString(9));
                e.setDeptid(Integer.parseInt(res.getString(10)));
                e.setRoleid(Integer.parseInt(res.getString(11)));
                e.setUsername(res.getString(12));
                e.setPassword(res.getString(13));
                e.setStatus(res.getString(14));
                return true;
            }
        }catch (Exception ex){
            Logger.getLogger(EmployeeLogic.class.getName()).log(Level.SEVERE, null, ex);
            //ex.printStackTrace();
        }
        return false;
    }
    
    public boolean addEmployee(Employee e){
        String query = "INSERT INTO employee (FirstName, MiddleName, LastName, DOB, Gender, Address, Contact, Email, Dept_ID, Role_ID, UserName, Password, Status) VALUES ('"+e.getFname()+"','"+e.getMname()+"','"+e.getLname()+"','"+e.getDOB()+"','"+e.getGender()+"','"+e.getAddress()+"','"+e.getContact()+"','"+e.getEmail()+"','"+e.getDeptid()+"','"+e.getRoleid()+"','"+e.getUsername()+"','"+e.getPassword()+"','P')";
        return conn.iud(query)>0;
    }
    
    public boolean acceptEmployee(Employee e){
        String query = "UPDATE employee SET Status = 'A'";
        //String query = "INSERT INTO hr (FirstName, MiddleName, LastName, DOB, Gender, Address, Contact, Email, Dept_ID, UserName, Password, Status) VALUES ('"+hr.getFname()+"','"+hr.getMname()+"','"+hr.getLname()+"','"+hr.getDOB()+"','"+hr.getGender()+"','"+hr.getAddress()+"','"+hr.getContact()+"','"+hr.getEmail()+"','"+hr.getDeptid()+"','"+hr.getUsername()+"','"+hr.getPassword()+"','P')";
        return conn.iud(query)>0;
    }
    
    public ArrayList<String[]> viewEmployee(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Role_ID,Status FROM employee");
        //ResultSet res = conn.select("SELECT * FROM employee");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex) {
            //Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return data;
        }
    }
    
    // An employee is not alllowed to view other' detail
    public ArrayList<String[]> viewSelectedEmployee(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Role_ID,Status FROM employee WHERE Status = 'A'");
        //ResultSet res = conn.select("SELECT * FROM employee");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex) {
            //Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return data;
        }
    }
        public ArrayList<String[]> viewNotSelectedEmployee(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Role_ID,Status FROM employee WHERE Status = 'P'");
        //ResultSet res = conn.select("SELECT * FROM employee");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex) {
            //Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return data;
        }
    }
    
    public ArrayList<String[]> viewSelf(Employee e, int empid){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT * FROM employee WHERE Emp_ID = '"+empid+"'");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12),res.getString(13),res.getString(14)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }
    
    public ArrayList<String[]> SearchEmployee(String name){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Role_ID,Status FROM employee WHERE FirstName LIKE '%"+name+"%'");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }
    
    public ArrayList<String[]> SearchApprovedEmployee(String name){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Role_ID,Status FROM employee WHERE FirstName LIKE '%"+name+"%' AND Status = 'A'");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }
    
    public ArrayList<String[]> SearchPendingEmployee(String name){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Role_ID,Status FROM employee WHERE FirstName LIKE '%"+name+"%' AND Status = 'P'");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }
    
    public boolean updateEmployee(Employee e){
        String query = "UPDATE employee SET FirstName = '"+e.getFname()+"', MiddleName = '"+e.getMname()+"', LastName = '"+e.getLname()+"', DOB = '"+e.getDOB()+"', Gender = '"+e.getGender()+"', Address = '"+e.getAddress()+"', Contact = '"+e.getContact()+"', Email = '"+e.getEmail()+"', UserName = '"+e.getUsername()+"', Password = '"+e.getPassword()+"' WHERE Emp_ID = '"+e.getEmpid()+"'";
        JOptionPane.showMessageDialog(null, "Your respective Employee Account has been Updated");
        return conn.iud(query)>0;
    }
    
    public boolean updateEmployeeStatus(Employee e){
        String query = "UPDATE employee SET Dept_ID = '"+e.getDeptid()+"', Role_ID = '"+e.getRoleid()+"', Status = '"+e.getStatus()+"' WHERE Emo_ID - '"+e.getEmpid()+"'";
        JOptionPane.showMessageDialog(null, "The respective Employee's Account has been Updated");
        return conn.iud(query)>0;
    }
    
    public boolean acceptEmployee(Employee e, String empid){
        //String query = "DELETE FROM employee, emp_role USING employee INNER JOIN emp_role ON employee.Emp_ID = emp_role.Emp_ID WHERE employee.Emp_ID = '"+empid+"'";
        String query = "UPDATE employee SET Status = 'A' WHERE Emp_ID = '"+empid+"'";
        JOptionPane.showMessageDialog(null, "The respective Employee's Account Request has been Accepted");
        return conn.iud(query)>0;
    }
    
    public boolean revokeEmployee(Employee e, String empid){
        //String query = "DELETE FROM employee, emp_role USING employee INNER JOIN emp_role ON employee.Emp_ID = emp_role.Emp_ID WHERE employee.Emp_ID = '"+empid+"'";
        String query = "UPDATE employee SET Status = 'P' WHERE Emp_ID = '"+empid+"'";
        JOptionPane.showMessageDialog(null, "The respective Employee's Account has been Revoked");
        return conn.iud(query)>0;
    } 
    
    public boolean removeEmployee(Employee e, String empid){
        String query = "DELETE FROM employee WHERE Emp_ID = '"+empid+"'";
        JOptionPane.showMessageDialog(null, "The respective Employee's Account has been Completely Removed");
        return conn.iud(query)>0;
    } */
    
}
