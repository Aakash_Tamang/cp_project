/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Controller;

import com.EmployeeManagementSystem.Model.Admin;
import com.EmployeeManagementSystem.Model.Employee;
import com.EmployeeManagementSystem.Model.HR;
import com.EmployeeManagementSystem.Model.Role;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class AdminLogic {
    
    Admin ad;
    
    public AdminLogic(Admin ad) {
        this.ad = ad;
    }
    
    public boolean adLogin() throws Exception{
        ResultSet res = ad.adLogin();
        try{
            if(res.next()){
                ad.setAid(Integer.parseInt(res.getString(1)));
                ad.setUsername(res.getString(2));
                ad.setUsername(res.getString(3));
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        return false;
    }
    
    public boolean updateAdmin(){
        int res=ad.updateAdmin();
        return res>0;
    }
    
    public ArrayList<String []> viewSelf(){
        ResultSet res;
        res = ad.viewSelf();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1),res.getString(2),res.getString(3)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    /*private DbConnection conn;    
    public AdminLogic(){
        conn = DbConnection.getInstance();
    }    
    private int id;    
    public boolean loginAdmin(Admin a){
        ResultSet res;
        String query = "SELECT * FROM admin WHERE UserName = '"+a.getUsername()+"' AND Password = '"+a.getPassword()+"'";
        res = conn.select(query);
        try{
            if(res.next()){
                a.setAid(Integer.parseInt(res.getString(1)));
                a.setUsername(res.getString(2));
                a.setPassword(res.getString(3));
                return true;
            }
        }catch (Exception ex){
            Logger.getLogger(AdminLogic.class.getName()).log(Level.SEVERE, null, ex);
            //ex.printStackTrace();
        }
        return false;
    }
    
    public boolean updateAdmin (Admin a){
        //String query = "UPDATE admin SET UserName = '"+a.getUsername()+"', Password = '"+a.getPassword()+"' WHERE Admin_ID = '"+a.getAid()+"' ";
        String query = "UPDATE admin SET UserName = '"+a.getUsername()+"', Password = '"+a.getPassword()+"'";        
        return conn.iud(query)>0;
    }
    
    public ArrayList<String[]> viewSelf(Admin ad, int aid){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT * FROM admin WHERE Admin_ID = '"+aid+"'");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }*/    
    
    
    /*public boolean acceptHR(HR hr){
        String query = "UPDATE hr SET Status = 'A'";
        //String query = "INSERT INTO hr (FirstName, MiddleName, LastName, DOB, Gender, Address, Contact, Email, Dept_ID, UserName, Password, Status) VALUES ('"+hr.getFname()+"','"+hr.getMname()+"','"+hr.getLname()+"','"+hr.getDOB()+"','"+hr.getGender()+"','"+hr.getAddress()+"','"+hr.getContact()+"','"+hr.getEmail()+"','"+hr.getDeptid()+"','"+hr.getUsername()+"','"+hr.getPassword()+"','P')";
        return conn.iud(query)>0;
    }*/
    
    /*public ArrayList<String[]> viewHR(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Status FROM hr");
        //ResultSet res = conn.select("SELECT * FROM hr");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex) {
            //Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return data;
        }
    }*/
    
    /*public ArrayList<String[]> viewAdmin(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT Admin_ID, UserName, Password FROM admin");
        //ResultSet res = conn.select("SELECT * FROM employee");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex) {
            //Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return data;
        }
    }*/   

    /*public ArrayList<String[]> SearchHR(String name){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Status FROM hr WHERE FirstName LIKE '"+name+"'");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }*/
    
    /*public ArrayList<String[]> SearchEmployee(String name){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT Emp_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Dept_ID,Status FROM employee WHERE FirstName LIKE '"+name+"'");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }*/
    
        // beacuse admin does not updates the HR
    /*public boolean updateHR(HR hr){
        String query = "UPDATE hr SET FirstName = '"+hr.getFname()+"', MiddleName = '"+hr.getMname()+"', LastName = '"+hr.getLname()+"', DOB = '"+hr.getDOB()+"', Gender = '"+hr.getGender()+"', Address = '"+hr.getAddress()+"', Contact = '"+hr.getContact()+"', Email = '"+hr.getEmail()+"', UserName = '"+hr.getUsername()+"', Password = '"+hr.getPassword()+"' WHERE HR_ID = '"+hr.getHrid()+"'";
        JOptionPane.showMessageDialog(null, "The respective HR's Account has been Updated");
        return conn.iud(query)>0;
    }*/
    
    /*public boolean updateEmployee(Employee emp){
        String query = "UPDATE employee e, emp_role er SET e.Dept_ID = '"+emp.getDeptid()+"', er.Role_ID = '"+emp.getRoleid()+"' WHERE e.Dept_ID = '"+emp.getDeptid()+"' AND er.Role_ID = '"+emp.getRoleid()+"'";
        JOptionPane.showMessageDialog(null, "The respective Employee's Account has been Updated");
        return conn.iud(query)>0;
    }*/
    
    /*public boolean removeHR(HR hr, String hrid){
        //String query = "DELETE FROM employee, emp_role USING employee INNER JOIN emp_role ON employee.Emp_ID = emp_role.Emp_ID WHERE employee.Emp_ID = '"+empid+"'";
        String query = "UPDATE hr SET Status = 'P'";
        JOptionPane.showMessageDialog(null, "The respective HR's Account has been Revoked");
        return conn.iud(query)>0;
    }*/
    
    /*public boolean removeEmployee(Employee emp, String empid){
        //String query = "DELETE FROM employee, emp_role USING employee INNER JOIN emp_role ON employee.Emp_ID = emp_role.Emp_ID WHERE employee.Emp_ID = '"+empid+"'";
        String query = "UPDATE employee SET Status = 'P'";
        JOptionPane.showMessageDialog(null, "The respective Employee's Account has been Revoked");
        return conn.iud(query)>0;
    }*/

    
    
    
}
