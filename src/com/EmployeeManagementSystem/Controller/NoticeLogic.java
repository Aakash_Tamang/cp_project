/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Controller;

import com.EmployeeManagementSystem.Model.HR;
import com.EmployeeManagementSystem.Model.Notice;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class NoticeLogic {
    
    Notice n;
    public NoticeLogic(Notice n){
        this.n = n;
    }
    
    public boolean addNewNotice(){
        return n.addNewNotice();
    }
    
    public ArrayList<String []> viewNotice(int id){
        ResultSet res;
        res = n.viewNotice(id);
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public ArrayList<String []> viewAllNotice(){
        ResultSet res;
        res = n.viewAllNotice();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public boolean updateNotice(){
        int res=n.updateNotice();
        return res>0;
    }
    
    public boolean removeNotice(String id){
        int res = n.removeNotice(id);
        return res>0;
    }
    
    /*
    private DbConnection conn;
    public NoticeLogic(){
        conn = DbConnection.getInstance();
    }
    
    public boolean addNewNotice(Notice n, HR hr){
        String query = "INSERT INTO notice (Notice_Subject, Notice, HR_ID, Date) Values ('"+n.getNotice_subject()+"','"+n.getNotice()+"', '"+hr.getHrid()+"', '"+n.getDate()+"')";
        return conn.iud(query)>0;
    }
    
    public ArrayList<String[]> viewAllNotice(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT notice.Notice_ID, hr.FirstName, hr.LastName, notice.HR_ID, notice.Notice_Subject, notice.Notice, notice.Date FROM notice FULL OUTER JOIN hr on notice.HR_ID = hr.HR_ID ORDER BY notice.Notice_ID");
        //ResultSet res = conn.select("SELECT Notice_ID, Notice_Subject, HR_ID from notice");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }
    
    public ArrayList<String[]> viewNotice(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT notice.Notice_ID, hr.FirstName, hr.LastName, notice.HR_ID, notice.Notice_Subject, notice.Notice, notice.Date FROM notice INNER JOIN hr on notice.HR_ID = hr.HR_ID");
        //ResultSet res = conn.select("SELECT Notice_ID, Notice_Subject, HR_ID from notice");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }
    
    
    
    public boolean updateNotice (Notice n){
        String query = "UPDATE notice SET Notice_Subject = '"+n.getNotice_subject()+"',SET Notice = '"+n.getNotice()+"', Date = '"+n.getDate()+"' WHERE Notice_ID = '"+n.getNoticeid()+"' ";
        JOptionPane.showMessageDialog(null, "The respective Notice has been Updated");
        return conn.iud(query)>0;
    }
    
    public boolean removeNotice(String noticeid, Notice n){
        //String query = "DELETE FROM role WHERE Role_ID = '"+d.getRoletid()+"'";
        String query = "DELETE FROM notice WHERE Notice_ID = '"+noticeid+"'";
        JOptionPane.showMessageDialog(null, "The respective Notice has been Removed");
        return conn.iud(query)>0;
    }*/
}
