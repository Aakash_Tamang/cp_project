/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Controller;

import com.EmployeeManagementSystem.Model.Employee;
import com.EmployeeManagementSystem.Model.Complain;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class ComplainLogic {
    
    Complain c;
    public ComplainLogic(Complain c){
        this.c = c;
    }
    
    public boolean addNewComplain(){
        return c.addNewComplain();
    }
    
    public ArrayList<String []> viewAllComplain(){
        ResultSet res;
        res = c.viewAllComplain();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7),res.getString(8),res.getString(9)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public ArrayList<String []> viewSentComplain(int id){
        ResultSet res;
        res = c.viewSentComplain(id);
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7),res.getString(8),res.getString(9)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public boolean updateComplain(){
        int res=c.updateComplain();
        return res>0;
    }  
    
    public boolean removeComplain(String id){
        int res = c.removeComplain(id);
        return res>0;
    }
    
    
    /*
    private DbConnection conn;
    public ComplainLogic(){
        conn = DbConnection.getInstance();
    }
    
    public boolean addNewComplain(Complain c, Employee e){
        String query = "INSERT INTO complain (Complain_Subject, Complain, Em_ID, Date) Values ('"+c.getComplain_subject()+"','"+c.getComplain()+"', '"+e.getEmpid()+"'";
        return conn.iud(query)>0;
    }
    
    public ArrayList<String[]> viewAllComplain(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT complain.Complain_ID, employee.FirstName, employee.LastName, complain.Emp_ID, employee.Dept_ID, employee.Role_ID, complain.Complain_Subject, complain.Complain, complain.Date FROM complain, employee");
        //ResultSet res = conn.select("SELECT Complain_ID, Complain_Subject, Emp_ID from complain");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3),res.getString(4), res.getString(5), res.getString(6),res.getString(7), res.getString(8), res.getString(9)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }
    
    public ArrayList<String[]> viewSentComplain(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT complain.Complain_ID, employee.FirstName, employee.LastName, complain.Emp_ID, employee.Dept_ID, employee.Role_ID, complain.Complain_Subject, complain.Complain, complain.Date FROM complain INNER JOIN employee ON complain.Emp_ID = employee.Emp_ID");
        //ResultSet res = conn.select("SELECT Complain_ID, Complain_Subject, Emp_ID from complain");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3),res.getString(4), res.getString(5), res.getString(6),res.getString(7), res.getString(8), res.getString(9)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }
    
    
    
    public boolean updateComplain (Complain c){
        String query = "UPDATE complain SET Complain_Subject - '"+c.getComplain_subject()+"',Complain = '"+c.getComplain()+"', Date = '"+c.getDate()+"' WHERE Complain_ID = '"+c.getComplainid()+"' ";
        JOptionPane.showMessageDialog(null, "The respective Notice has been Updated");
        return conn.iud(query)>0;
    }
    
    public boolean removeComplain(String complainid, Complain c){
        //String query = "DELETE FROM role WHERE Role_ID = '"+d.getRoletid()+"'";
        String query = "DELETE FROM complain WHERE Complain_ID = '"+complainid+"'";
        JOptionPane.showMessageDialog(null, "The respective Complain has been Removed");
        return conn.iud(query)>0;
    }*/
}
