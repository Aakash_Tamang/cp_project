/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Controller;

import com.EmployeeManagementSystem.Model.Department;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class DepartmentLogic {
    
    Department d;
    public DepartmentLogic(Department d){
        this.d = d;
    }
    
    public boolean addNewDepartment(){
        return d.addNewDepartment();
    }
    
    public ArrayList<String []> viewDepartment(){
        ResultSet res;
        res = d.viewDepartment();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public boolean updateDepartment(){
        int res=d.updateDepartment();
        return res>0;
    }
    
    /*public boolean revokeDepartment(String id){
        int res = d.revokeDepartment(id);
        return res>0;
    }*/
    
       
    
    /*
    private DbConnection conn;
    public DepartmentLogic(){
        conn = DbConnection.getInstance();
    }
    
    public boolean addNewDepartment(Department d){
        String query = "INSERT INTO department (Department) Values ('"+d.getDepartment()+"')";
        return conn.iud(query)>0;
    }
    
    public ArrayList<String[]> viewDepartment(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT * from department");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }   
    
    public boolean updateDepartment (Department d){
        String query = "UPDATE department SET Department = '"+d.getDepartment()+"' WHERE Dept_ID = '"+d.getDeptid()+"' ";
        JOptionPane.showMessageDialog(null, "The respective Department has been Updated");
        return conn.iud(query)>0;
    }
    
    public boolean removeDepartment(String deptid, Department d){
        //String query = "DELETE FROM department WHERE Dept_ID = '"+d.getDeptid()+"'";
        String query = "DELETE FROM department WHERE Dept_ID = '"+deptid+"'";
        JOptionPane.showMessageDialog(null, "The respective Department has been Removed");
        return conn.iud(query)>0;
    }*/
}
