/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Controller;

import com.EmployeeManagementSystem.Model.Employee;
import com.EmployeeManagementSystem.Model.HR;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class HRLogic {
    
    HR hr;
    public HRLogic(HR hr){
        this.hr = hr;
    }
    
    public boolean hrLogin() throws Exception{
        ResultSet res = hr.hrLogin();
        try{
            if(res.next()){
                hr.setHrid(Integer.parseInt(res.getString(1)));
                hr.setFname(res.getString(2));
                hr.setMname(res.getString(3));
                hr.setLname(res.getString(4));
                hr.setDOB(res.getString(5));
                hr.setGender(res.getString(6));
                hr.setAddress(res.getString(7));
                hr.setContact(res.getString(8));
                hr.setEmail(res.getString(9));
                hr.setUsername(res.getString(10));
                hr.setPassword(res.getString(11));
                hr.setStatus(res.getString(12));
                return true;
            }
        }catch(SQLException ex){
            return false;
        }
        return false;
    }
    
    public boolean updateHR(){
        int res=hr.updateHR();
        return res>0;
    }
    
    public ArrayList<String []> viewSelf(){
        ResultSet res;
        res = hr.viewSelf();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public boolean addHR(){
        System.out.println("A");
        return hr.addHR();
    }
    
    public boolean acceptHR(String hrid){
        System.out.println("The ID is "+hrid);
        int res = hr.acceptHR(hrid);
        return res>0;
    }
    
    public ArrayList<String []> viewHR(){
        ResultSet res;
        res = hr.viewHR();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    public ArrayList<String []> viewSelectedHR(){
        ResultSet res;
        res = hr.viewSelectedHR();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public ArrayList<String []> viewNotSelectedHR(){
        ResultSet res;
        res = hr.viewNotSelectedHR();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public ArrayList<String []> searchHR(String name){
        ResultSet res;
        res = hr.searchHR(name);
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            System.out.println(ex.getMessage());            
        }
        return null;
    }
    
    public boolean revokeHR(String id){
        int res = hr.revokeHR(id);
        return res>0;
    }
    
    /*public boolean removeHR(String id){
        int res = hr.removeHR(id);
        return res>0;
    }*/
    /*
    private DbConnection conn;    
    public HRLogic(){
        conn = DbConnection.getInstance();
    }    
    private int id;    
    public boolean loginHR(HR hr){
        ResultSet res;
        String query = "SELECT * FROM hr WHERE UserName = '"+hr.getUsername()+"' AND Password = '"+hr.getPassword()+"'";
        res = conn.select(query);
        try{
            if(res.next()){
                hr.setHrid(Integer.parseInt(res.getString(1)));
                hr.setFname(res.getString(2));
                hr.setMname(res.getString(3));
                hr.setLname(res.getString(4));
                hr.setDOB(res.getString(5));
                hr.setGender(res.getString(6));
                hr.setAddress(res.getString(7));
                hr.setContact(res.getString(8));
                hr.setEmail(res.getString(9));
                hr.setUsername(res.getString(10));
                hr.setPassword(res.getString(11));
                hr.setStatus(res.getString(12));
                return true;
            }
        }catch (Exception ex){
            Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            //ex.printStackTrace();
        }
        return false;
    }    
    public boolean addHR(HR hr){
        String query = "INSERT INTO hr (FirstName, MiddleName, LastName, DOB, Gender, Address, Contact, Email, UserName, Password, Status) VALUES ('"+hr.getFname()+"','"+hr.getMname()+"','"+hr.getLname()+"','"+hr.getDOB()+"','"+hr.getGender()+"','"+hr.getAddress()+"','"+hr.getContact()+"','"+hr.getEmail()+"','"+hr.getUsername()+"','"+hr.getPassword()+"','P')";
        return conn.iud(query)>0;
    }    
    public boolean acceptHR(HR hr){
        String query = "UPDATE hr SET Status = 'A'";
        //String query = "INSERT INTO hr (FirstName, MiddleName, LastName, DOB, Gender, Address, Contact, Email, Dept_ID, UserName, Password, Status) VALUES ('"+hr.getFname()+"','"+hr.getMname()+"','"+hr.getLname()+"','"+hr.getDOB()+"','"+hr.getGender()+"','"+hr.getAddress()+"','"+hr.getContact()+"','"+hr.getEmail()+"','"+hr.getDeptid()+"','"+hr.getUsername()+"','"+hr.getPassword()+"','P')";
        return conn.iud(query)>0;
    }    
    public ArrayList<String[]> viewHR(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Status FROM hr");        
        //ResultSet res = conn.select("SELECT * FROM hr");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex) {
            //Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return data;
        }
    }    
    public ArrayList<String[]> viewSelectedHR(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Status FROM hr WHERE Status = 'A'");        
        //ResultSet res = conn.select("SELECT * FROM hr");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex) {
            //Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return data;
        }
    }
    
        public ArrayList<String[]> viewNotSelectedHR(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Status FROM hr WHERE Status = 'P'");        
        //ResultSet res = conn.select("SELECT * FROM hr");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex) {
            //Logger.getLogger(HRLogic.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return data;
        }
    }    
    public ArrayList<String[]> viewSelf(HR hr, int hrid){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT * FROM hr WHERE HR_ID = '"+hrid+"'");
        try {
            if (res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10),res.getString(11),res.getString(12)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }    
    public ArrayList<String[]> SearchHR(String name){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT HR_ID, FirstName, MiddleName,LastName,DOB,Gender,Address,Contact,Email,Status FROM hr WHERE FirstName LIKE '%"+name+"%'");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8), res.getString(9), res.getString(10)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            ex.printStackTrace();
            return data;
        }
    }    
    public boolean updateHR(HR hr){
        String query = "UPDATE hr FirstName = '"+hr.getFname()+"', MiddleName = '"+hr.getMname()+"', LastName = '"+hr.getLname()+"', DOB = '"+hr.getDOB()+"', Gender = '"+hr.getGender()+"', Address = '"+hr.getAddress()+"', Contact = '"+hr.getContact()+"', Email = '"+hr.getEmail()+"', UserName = '"+hr.getUsername()+"', Password = '"+hr.getPassword()+"' WHERE HR_ID = '"+hr.getHrid()+"'";
        JOptionPane.showMessageDialog(null, "The respective HR's Account has been Updated");
        return conn.iud(query)>0;
    }    
    public boolean updateHRStatus(HR hr){
        String query = "UPDATE hr SET Status = '"+hr.getStatus()+"' WHERE HR_ID = '"+hr.getHrid()+"'";
        JOptionPane.showMessageDialog(null, "The respective HR's Account has been Updated");
        return conn.iud(query)>0;
    }    
    public boolean acceptHR(HR hr, String hrid){
        //String query = "DELETE FROM employee, emp_role USING employee INNER JOIN emp_role ON employee.Emp_ID = emp_role.Emp_ID WHERE employee.Emp_ID = '"+empid+"'";
        String query = "UPDATE hr SET Status = 'P'";
        JOptionPane.showMessageDialog(null, "The respective HR's Account Request has been Accepted");
        return conn.iud(query)>0;
    }    
    public boolean revokeHR(HR hr, String hrid){
        //String query = "DELETE FROM employee, emp_role USING employee INNER JOIN emp_role ON employee.Emp_ID = emp_role.Emp_ID WHERE employee.Emp_ID = '"+empid+"'";
        String query = "UPDATE hr SET Status = 'P'";
        JOptionPane.showMessageDialog(null, "The respective HR's Account has been Revoked");
        return conn.iud(query)>0;
    }    
    public boolean removeHR(HR hr, String hrid){
        //String query = "DELETE FROM employee, emp_role USING employee INNER JOIN emp_role ON employee.Emp_ID = emp_role.Emp_ID WHERE employee.Emp_ID = '"+empid+"'";
        String query = "DELETE FROM hr WHERE HRID = '"+hrid+"'";
        JOptionPane.showMessageDialog(null, "The respective HR's Account has been Completely Removed");
        return conn.iud(query)>0;
    } */   
    /*public boolean updateEmployee(Employee emp){
        String query = "UPDATE employee e, emp_role er SET e.Dept_ID = '"+emp.getDeptid()+"', er.Role_ID = '"+emp.getRoleid()+"' WHERE e.Dept_ID = '"+emp.getDeptid()+"' AND er.Role_ID = '"+emp.getRoleid()+"'";
        JOptionPane.showMessageDialog(null, "The respective Employee's Account has been Updated");
        return conn.iud(query)>0;
    }
    
    public boolean removeEmployee(Employee emp, String empid){
        //String query = "DELETE FROM employee, emp_role USING employee INNER JOIN emp_role ON employee.Emp_ID = emp_role.Emp_ID WHERE employee.Emp_ID = '"+empid+"'";
        String query = "UPDATE employee SET Status = 'P'";
        JOptionPane.showMessageDialog(null, "The respective Employee's Account has been Revoked");
        return conn.iud(query)>0;
    }*/
    
}
