/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.EmployeeManagementSystem.Controller;

import com.EmployeeManagementSystem.Model.Role;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Liverpool
 */
public class RoleLogic {
    
    Role r;
    public RoleLogic(Role r){
        this.r = r;
    }
    
    public boolean addNewRole(){
        return r.addNewRole();
    }
    
    public ArrayList<String []> viewRole(){
        ResultSet res;
        res = r.viewRole();
        ArrayList<String[]>data = new ArrayList<>();
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2)};
                data.add(data2);
            }return data;
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        return null;
    }
    
    public boolean updateRole(){
        int res=r.updateRole();
        return res>0;
    }
    
    /*public boolean revokeRole(String id){
        int res = d.revokeRole(id);
        return res>0;
    }*/  
    
    
    /*
    private DbConnection conn;
    public RoleLogic(){
        conn = DbConnection.getInstance();
    }
    
    public boolean addNewRole(Role r){
        String query = "INSERT INTO role (Role) Values ('"+r.getRole()+"')";
        return conn.iud(query)>0;
    }
    
    public ArrayList<String[]> viewRole(){
        ArrayList<String[]> data = new ArrayList<String[]>();
        ResultSet res = conn.select("SELECT * from role");
        try{
            while(res.next()){
                String[] data2 = {res.getString(1), res.getString(2)};
                data.add(data2);
            }
            return data;
        } catch (SQLException ex){
            //ex.printStackTrace();
            System.out.println("ERROR");
            return data;
        }
    }
    
    
    
    public boolean updateRole (Role r){
        String query = "UPDATE role SET Role = '"+r.getRole()+"' WHERE Role_ID = '"+r.getRoleid()+"' ";
        JOptionPane.showMessageDialog(null, "The respective Role has been Updated");
        return conn.iud(query)>0;
    }
    
    public boolean removeRole(String roleid, Role d){
        //String query = "DELETE FROM role WHERE Role_ID = '"+d.getRoletid()+"'";
        String query = "DELETE FROM role WHERE Role_ID = '"+roleid+"'";
        JOptionPane.showMessageDialog(null, "The respective Role has been Removed");
        return conn.iud(query)>0;
    }*/
}
